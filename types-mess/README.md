# Installation
> `npm install --save @types/mess`

# Summary
This package contains type definitions for mess 0.1.2 (https://github.com/bobrik/node-mess).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/types-2.0/mess

Additional Details
 * Last updated: Mon, 19 Sep 2016 17:28:59 GMT
 * File structure: ProperModule
 * Library Dependencies: none
 * Module Dependencies: none
 * Global values: shuffle

# Credits
These definitions were written by Wim Looman <https://github.com/Nemo157>.
